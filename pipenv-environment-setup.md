# General Instructions

## Python Installation

[Download here](https://www.python.org/downloads/macos/)

[Python 3.11.4 macOS 64-bit universal2 installer](https://www.python.org/ftp/python/3.11.4/python-3.11.4-macos11.pkg)

After installation you will see the different Python versions in the `/Applications`

## Pipenv Installation

[Pipenv Video Tutorial](https://www.youtube.com/watch?v=zDYL22QNiWk)

[Pipenv](https://pipenv.pypa.io/en/latest/) is a Python virtualenv management tool that supports a multitude of systems and nicely bridges the gaps between pip, python (using system python, pyenv or asdf) and virtualenv. `pip` + `virtualenv` working together.

**Python installation check**

```
% which pip3
/Library/Frameworks/Python.framework/Versions/3.9/bin/pip3

% pip3 --version
pip 22.0.4 from /Library/Frameworks/Python.framework/Versions/3.9/lib/python3.9/
site-packages/pip (python 3.9)

project-folder % which python3
/Library/Frameworks/Python.framework/Versions/3.9/bin/python3

william.rena@AMAT9P4KJPK2X project-folder % python3 --version
Python 3.9.13
```

[Virtual Environment Installation](https://pipenv.pypa.io/en/latest/installation/)

Creating the virtual environment **(will create the Pipfile)**

```
% pip3 install pipenv
	
% which pipenv
/Library/Framework/Python.framework/Versions/3.11/pipenv
	
% mkdir project-folder
% cd project-folder
% pwd
/Users/william.rena/.project/project-folder
	
project-folder % pipenv --python 3.11
```

**Testing the virtual environment**

```
project-folder % pipenv shell

(project-folder) % python
Python 3.11.4
>>> import sys
>>> sys.executable
'/Users/william.rena/.local/share/virtualenvs/project-folder-je41MXKR/bin/python'
>>> exit()

(project-folder) % exit
project-folder %
```

**Installing the requirements**

```
project-folder % pipenv install -r ~/.project/project-folder/python/requirements.txt
project-folder % pipenv graph

project-folder % pipenv run python
project-folder % pipenv run python python/validate-image.py

project-folder % pipenv lock -r

project-folder % pipenv --rm
project-folder % pipenv install

project-folder % pipenv check
```

**How do we get the Python path?**


```
project-folder % pwd
/Users/william.rena/.project/project-folder
	
project-folder % pipenv --where
/Users/william.rena/.project/project-folder
	
project-folder % pipenv --version
pipenv, version 2023.6.18
	
project-folder % pipenv --py
/Users/william.rena/.local/share/virtualenvs/project-folder-76hsswaK/bin/python
```
	
```
project-folder % pipenv shell
Launching subshell in virtual environment...
Restored session: Thu Jun 22 17:31:01 PST 2023
 . /Users/william.rena/.local/share/virtualenvs/project-folder-76hsswaK/bin/activate                                                           
william.rena@AMAT9P4KJPK2X project-folder %  . /Users/william.rena/.local/share/virtualenvs/project-folder-76hsswaK/bin/activate

(project-folder) project-folder % python
Python 3.11.4 (v3.11.4:d2340ef257, Jun  6 2023, 19:15:51) [Clang 13.0.0 (clang-1300.0.29.30)] on darwin
Type "help", "copyright", "credits" or "license" for more information.
>>> import sys
>>> sys.executable
'/Users/william.rena/.local/share/virtualenvs/project-folder-76hsswaK/bin/python'
>>> exit()

(project-folder) project-folder % exit

Saving session...
...saving history...truncating history files...
...completed.

project-folder % 
```

**Python Path:**
`/Users/william.rena/.local/share/virtualenvs/project-folder-76hsswaK/bin/python`

## How to pip install for python2 and python3

	$ python3 -m pip install -U --force-reinstall pip
	$ python -m pip install -U --force-reinstall pip

## ~/.bash_profile

```
# Setting PATH for Python 3.7
# The original version is saved in .bash_profile.pysave
PATH="/Library/Frameworks/Python.framework/Versions/3.7/bin:${PATH}"
export PATH

# mysql
export PATH=$PATH:/usr/local/cellar/mysql@5.6/5.6.46_2/bin
export DYLD_LIBRARY_PATH=/usr/local/cellar/mysql@5.6/5.6.46_2/lib/
export LDFLAGS='-I/usr/local/opt/openssl/include -L/usr/local/opt/openssl/lib'

# virtual env
export WORKON_HOME=~/.venvs
export VIRTUALENVWRAPPER_PYTHON=~/.venvs/tt/bin/python
source ~/.venvs/tt/bin/virtualenvwrapper.sh

# Nodejs
export PATH="/usr/local/opt/icu4c/bin:$PATH"
export PATH="/usr/local/opt/icu4c/sbin:$PATH"
export PATH="/usr/local/bin:$PATH"
export NODE_PATH="/usr/local/lib/node_modules"

# nvm
export NVM_DIR="~/.nvm"
source $(brew --prefix nvm)/nvm.sh
```