import cv2
import os
import numpy
import argparse
import image_similarity_measures

from json import dumps
from sys import argv
from image_similarity_measures.quality_metrics import ssim

# code reference here.
# https://betterprogramming.pub/how-to-measure-image-similarities-in-python-12f1cb2b7281

SSIM_ACCEPTABLE_VALUE = 0.98

numpy.seterr(divide='ignore', invalid='ignore')

parser = argparse.ArgumentParser()

# _images_ca
parser.add_argument("--market_dir", help="Market Root Image Directory", type=str, required=True)

# promotions
parser.add_argument("--image_dir", help="Image Subdirectory", type=str, required=True)

# promotions-2-you-can-enjoy-eating-vegetables-detail-header.png 
parser.add_argument("--image_identifier", help="Image Identifier", type=str, required=True)

# handle data for the results
parser.add_argument("--write_results", help="Writing the results", type=int, default=0)

# debug enabled
parser.add_argument("--debug", help="Debug enabled", type=bool, default=False)

args = parser.parse_args()

# ------------

print("[Python] evaluate-image-matches.py")

def calc_closest_val(dict, checkMax):
    result = {}
    if (checkMax):
    	closest = max(dict.values())
    else:
    	closest = min(dict.values())
    		
    for key, value in dict.items():
    	if (value == closest):
    	    result[key] = closest

    return result

if args.market_dir and args.image_dir and args.image_identifier:
	market_folder = args.market_dir
	original_images_folder = args.image_dir + "-reference"
	predicted_images_folder = args.image_dir

	original_image = market_folder + "/" + original_images_folder + "/" + args.image_identifier
	predicted_image = market_folder + "/" + predicted_images_folder + "/" + args.image_identifier

	original_file_exist = os.path.exists(original_image)
	predicted_file_exist = os.path.exists(predicted_image)

	if args.debug:
		print("[Python] Write Results: " + str(args.write_results))
		print("[Python] Working Directory: " + str(os.getcwd()))
		print("[Python] Image: [" + args.image_identifier + "]")
		print("[Python] Original-image: " + original_image + ", " + str(original_file_exist))
		print("[Python] Predicted-image: " + predicted_image + ", " + str(predicted_file_exist))

	if original_file_exist and predicted_file_exist:
		test_img = cv2.imread(original_image)

		ssim_measures = {}

		scale_percent = 100 # percent of original img size
		width = int(test_img.shape[1] * scale_percent / 100)
		height = int(test_img.shape[0] * scale_percent / 100)
		dim = (width, height)

		img_path = predicted_image
		data_img = cv2.imread(img_path)
		resized_img = cv2.resize(data_img, dim, interpolation = cv2.INTER_AREA)

		ssim_measures[img_path] = ssim(test_img, resized_img)
		ssim = calc_closest_val(ssim_measures, True)

		result_summary = {}
		result_summary.update({
			'ssim': ssim.get(img_path),
			'acceptable_value': SSIM_ACCEPTABLE_VALUE,
			'range': '>=' + str(SSIM_ACCEPTABLE_VALUE)
		})
		
		if args.write_results == 0:
			print(dumps(result_summary))
		else:
			print(dumps(result_summary), file = open(3, "w"))
		