import logging
import argparse

parser = argparse.ArgumentParser()
parser.add_argument("--type", help="Log type", type=str, choices=['DEBUG', 'INFO', 'ERROR'], required=True)
parser.add_argument("--message", help="Message", type=str, required=True)

args = parser.parse_args()

# ------------

logger = logging.getLogger(__name__)
logger.setLevel(logging.DEBUG)

formatter = logging.Formatter('%(asctime)s: %(message)s')

file_handler = logging.FileHandler('logging-ar2020.log')
file_handler.setFormatter(formatter)
file_handler.setLevel(logging.DEBUG)

stream_handler = logging.StreamHandler()
stream_handler.setFormatter(formatter)

logger.addHandler(file_handler)
logger.addHandler(stream_handler)

if args.type == 'INFO':
	logger.info(args.message)
elif args.type == 'ERROR':
	logger.error(args.message)
elif args.type == 'DEBUG':
	logger.debug(args.message)
