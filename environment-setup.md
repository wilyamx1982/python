# General Instructions

## Python Installation

[Download here](https://www.python.org/downloads/macos/)

[Python 3.7.6 - Dec. 18, 2019 for macOS 64-bit installer](https://www.python.org/ftp/python/3.7.6/python-3.7.6-macosx10.9.pkg)

After installation you will see the different Python versions in the `/Applications`

Recommend to install **virtualenvwrapper** afterwards.

## How to pip install for python2 and python3

	$ python3 -m pip install -U --force-reinstall pip
	$ python -m pip install -U --force-reinstall pip

## Create environment using [VirtualEnvWrapper](http://virtualenvwrapper.readthedocs.io/) in MacOSX

**python installation**

```
$ which python3
/Library/Frameworks/Python.framework/Versions/3.7/bin/python3

$ which pip3
/Library/Frameworks/Python.framework/Versions/3.7/bin/pip3

$ pip3 install virtualenvwrapper===6.0.0.0a1
Collecting virtualenvwrapper...
```

**~/.bash_profile**

```
# Setting PATH for Python 3.7
# The original version is saved in .bash_profile.pysave
PATH="/Library/Frameworks/Python.framework/Versions/3.7/bin:${PATH}"
export PATH

# virtualenvwrapper
export WORKON_HOME=~/.venvs
export VIRTUALENVWRAPPER_PYTHON=/Library/Frameworks/Python.framework/Versions/3.7/bin/python3.7
source /usr/local/bin/virtualenvwrapper.sh
```

**mkvirtualenv**

```
$ source ~/.bash_profile

$ mkvirtualenv ~/.venvs/ar2020 --python=python3.9
$ workon ar2020
(ar2020) $ setvirtualenvproject $VIRTUAL_ENV ~/.vitality/ar2020
$ workon ar2020
$ pwd
/Users/wrena/_GIT/ThinkItTwice/api
```

```
$ mkvirtualenv ~/.venvs/tt-api --python=python3
$ workon tt-api
(tt-api) $ setvirtualenvproject $VIRTUAL_ENV ~/_GIT/ThinkItTwice/api
$ workon tt-api
$ pwd
/Users/wrena/_GIT/ThinkItTwice/api
```

**more examples**

```
$ virtualenv -p python ~/.venvs/tt
$ source ~/.venvs/tt/bin/activate

$ sudo pip install flake8
$ sudo pip install virtualenvwrapper
$ which virtualenvwrapper.sh
/Users/wrena/.venvs/tt/bin/virtualenvwrapper.sh

# ~/.bash_profile
$ export WORKON_HOME=~/.venvs
$ export VIRTUALENVWRAPPER_PYTHON=~/.venvs/tt/bin/python
$ source ~/.venvs/tt/bin/virtualenvwrapper.sh

# ~/.bash_profile
$ export WORKON_HOME=~/.venvs
$ export VIRTUALENVWRAPPER_PYTHON=/usr/bin/python
$ source /usr/local/bin/virtualenvwrapper.sh

# source
$ source ~/.bash_profile

# removing the virtual-env
$ rmvirtualenv tt-api

# create separate virtualenv and assign working directory
$ mkvirtualenv ~/.venvs/tt-api --python=python
$ mkvirtualenv ~/.venvs/tt-api3 --python=python3
$ mkvirtualenv ~/.venvs/tt-api3.6 --python=python3.6

$ workon tt-api
(tt-api) $ setvirtualenvproject $VIRTUAL_ENV /Users/wrena/Documents/_GIT/ThinkItTwice/api
(tt-api) $ setvirtualenvproject $VIRTUAL_ENV /william/_git/thinkittwice/api
(tt-api) $ setvirtualenvproject $VIRTUAL_ENV /users/shared/relocated_items/security/william/_git/thinkittwice/api

(tt-api) $ pwd
/william/_git/thinkittwice/api
/users/shared/relocated_items/security/william/_git/thinkittwice/api

# or change .project file
~/.venvs/tt-api/.project
```

**How to verify correct configurations?**

```
(tt-app-server) earvin-pc:app-server wrena$ pwd
/william/_git/thinkittwice/app-server

(tt-app-server) earvin-pc:app-server wrena$ which python
/Users/wrena/.venvs/tt-app-server/bin/python

(tt-app-server) earvin-pc:app-server wrena$ python --version
Python 2.7.10

(tt-app-server) earvin-pc:app-server wrena$ which pip
/Users/wrena/.venvs/tt-app-server/bin/pip

(tt-app-server) earvin-pc:app-server wrena$ pip --version
pip 18.1 from /Users/wrena/.venvs/tt-app-server/lib/python2.7/site-packages/pip (python 2.7)
```

**Suggested (virtual env : working directories / git repo)**

```
$ workon
tt-api
tt-app-server
tt-cms-spa
tt-cms
tt-django-albert

(tt-api) earvin-pc:api wrena$ pwd
/william/_git/thinkittwice/api

(tt-app-server) earvin-pc:app-server wrena$ pwd
/william/_git/thinkittwice/app-server

(tt-django-albert) earvin-pc:django-albert wrena$ pwd
/william/_git/thinkittwice/django-albert

(tt-cms) earvin-pc:cms wrena$ pwd
/william/_git/thinkittwice/cms

(tt-cms-spa) earvin-pc:cms-spa wrena$ pwd
/william/_git/thinkittwice/cms-spa
```

## ~/.bash_profile

```
# Setting PATH for Python 3.7
# The original version is saved in .bash_profile.pysave
PATH="/Library/Frameworks/Python.framework/Versions/3.7/bin:${PATH}"
export PATH

# mysql
export PATH=$PATH:/usr/local/cellar/mysql@5.6/5.6.46_2/bin
export DYLD_LIBRARY_PATH=/usr/local/cellar/mysql@5.6/5.6.46_2/lib/
export LDFLAGS='-I/usr/local/opt/openssl/include -L/usr/local/opt/openssl/lib'

# virtual env
export WORKON_HOME=~/.venvs
export VIRTUALENVWRAPPER_PYTHON=~/.venvs/tt/bin/python
source ~/.venvs/tt/bin/virtualenvwrapper.sh

# Nodejs
export PATH="/usr/local/opt/icu4c/bin:$PATH"
export PATH="/usr/local/opt/icu4c/sbin:$PATH"
export PATH="/usr/local/bin:$PATH"
export NODE_PATH="/usr/local/lib/node_modules"

# nvm
export NVM_DIR="~/.nvm"
source $(brew --prefix nvm)/nvm.sh
```