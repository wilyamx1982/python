# Coding Techniques

1. **Dictionary comprehension**

		high_m_by_id = {m.id: m.value for m in measurements if m.value > 70}
		
2. **Set comprehension**

		high_m_by_id = {m.value for m in measurements if m.value > 70}
		
3. **List comprehension**

		high_measurements = [
			m.value
			for m in measurements
			if m.value > 70
		]
		
4. Generator expression

		high_measurements = (
			m.value
			for m in measurements
			if m.value > 70
		)
		
5. **Merging dictionaries** (python 3.5+)

		m4 = {**query, **route, **post}
		
6. `timezone.now(`) is dependent to `settings.TIME_ZONE = ‘UTC’`

7. avoid **_underscore** in URLs

8. get the **key-value pairs** (dictionary comprehension)

		{ key: dictionary.get(key) for key in [‘key_1’, ‘key_2’] }
		result: { ‘key_1’: 77L, ‘key_2’: 'EN' }
		
9. **Union of dictionaries**

		dict(a, **b)
		
10. Creating a manager with QuerySet methods

		class Person(models.Model):
    		...
    		objects = PersonQuerySet.as_manager()
	    		
11. `_, self.bucket = self.created_s3_buckets()`

12. To sort the list in place...

		ut.sort(key=lambda x: x.count, reverse=True)
		
13. To return a new list, use the sorted() built-in function...

		newlist = sorted(ut, key=lambda x: x.count, reverse=True)
		newlist = sorted(lista, key=len, reverse=True)
